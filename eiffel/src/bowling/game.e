note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME
create
	make

feature
	n_birilli: INTEGER		-- numero di birilli
	points: INTEGER			-- punteggio
	n_roll: INTEGER			-- puntatore array
	frame: INTEGER			-- frame gioco
	rolls: ARRAY[INTEGER]	-- numero di tiri

	make
	do
		n_birilli := 10
		points := 0
		n_roll := 0
		frame:= 0
		create rolls.make(0,21)
	end

feature
	roll(birilli_ko: INTEGER)

	require
		too_many_p: birilli_ko <= n_birilli
		not_negative: birilli_ko >= 0
		error_rolls: n_roll <= 21
		frame_error: frame <= 10

	do

		n_birilli := n_birilli - birilli_ko

		rolls.put(birilli_ko, n_roll)

		if birilli_ko = 10 then
			if (n_roll\\2)=0 and frame < 9 then
				n_roll := n_roll + 1
				rolls.put(0, n_roll)
			end
		end

		n_roll := n_roll + 1

		if n_birilli=0 then
			n_birilli := 10
		end

		frame:= frame + ((n_roll-1)\\2)

		ensure
			birilli_rimasti: n_birilli >= 0
	end


feature{NONE}
	spareEvent(base: INTEGER) : INTEGER
	do
		points:= points + rolls.at(base+2)
		Result:= points + 10
	end


feature{NONE}
	strikeEvent(in_frame:INTEGER; base: INTEGER) : INTEGER
	do

		if in_frame /= frame-1 then
			if rolls.at(base+3) /=0 then
				points:= points + rolls.at(base+2) + rolls.at(base+3)
			else
				points:= points + rolls.at(base+2) + rolls.at(base+4)
			end
		else
			points:= points + rolls.at(base+1) + rolls.at(base+2)
		end


		Result:= points + 10
	end


feature
	score : INTEGER
	local
		i: INTEGER
		in_frame: INTEGER

	do
		in_frame:=0

		from in_frame:= 0
		until in_frame = frame
		loop

			if (i\\2)=0 and rolls.at(i) = 10 then

				points:= strikeEvent(in_frame, i)
				i:= i+1

			elseif (i\\2)=0 and rolls.at(i)+rolls.at(i+1) = 10 then

				points:= spareEvent(i)
				i:= i+1

			else
				points:= points + rolls.at(i)
			end

			i:= i+1
			in_frame:= in_frame + ((i-1)\\2)
		end
		Result := points

		ensure
			score_error: points <= 300
	end
end
