note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

redefine on_prepare end

feature {NONE}
	g: GAME

feature -- Test routines
	on_prepare
	do
		create g.make
		check g /= Void end
	end

feature
	testGutterGame
	do
		g.roll(0)
		assert("testGutterGame", g.score()=0)
	end

feature
	testAllOnes
	local
		i: INTEGER
	do
		from i:=0
		until i = 20
		loop
			g.roll(1)
			i := i + 1
		end
		assert("testAllOnes", g.score=20)
	end


feature
	testOneSpares
	local
		i: INTEGER
	do
		g.roll(5)
		g.roll(5)
		g.roll(3)
		from i := 0
		until i = 17
		loop
			g.roll(0)
			i := i+1
		end
		assert("testOneSpares", g.score=16)
	end

feature
	testOneStrike
	local
		i: INTEGER
	do
		g.roll(10)
		g.roll(3)
		g.roll(4)
		from i := 0
		until i = 16
		loop
			g.roll(0)
			i := i+1
		end
		assert("testOneStrike", g.score=24)
	end

feature
	testPerfectGame
	local
		i: INTEGER
	do
		from i := 0
		until i = 12
		loop
			g.roll(10)
			i := i+1
		end
		assert("testPerfectGame", g.score=300)
	end

feature
	testLastSpare
	local
		i: INTEGER
	do
		from i := 0
		until i = 9
		loop
			g.roll(10)
			i := i+1
		end

		g.roll(5)
		g.roll(5)
		g.roll(10)

		assert("testLastSpare", g.score=275)
	end

end


